'use strict';
var cluster = require('cluster');
if (cluster.isMaster) {

    // Count the machine's CPUs
    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    // Code to run if we're in a worker process
} else {
    var express = require('express'),
        logger = require('morgan'),
        cookieParser = require('cookie-parser'),
        bodyParser = require('body-parser'),
        methodOverride = require('method-override'),
        session = require('express-session'),
        path = require('path'),
        ejs = require('ejs'),
        Validator = require('validator'),
        axios = require('axios');

    // Create a new Express application
    var app = express();
    app.engine('html', ejs.renderFile);
    app.use(logger('combined'));
    app.use(cookieParser());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use(methodOverride('X-HTTP-Method-Override'));
    app.use(session({ secret: 'RhWPzhulwE5EaA51fYSQaqKqiWZO6Qjv', saveUninitialized: true, resave: true }));
    app.use(express.static(path.join(__dirname, './public/')));
    app.set('views', path.join(__dirname, './public/'));
    app.set('view engine', 'html');
    var router = express.Router();
    router
        .get('/', function(req, res) {
            console.log('Worker %d executed!', cluster.worker.id);
            res.render('index.html');
        })
        .get('/login', function(req, res) {
            res.redirect('/#/login');
        })
        .get('/register', function(req, res) {
            res.redirect('/#/register');
        })
        .post('/login', function(req, res) {
            res.redirect('/#/login');
        })
        .post('/register', function(req, res) {
            res.redirect('/#/register');
        })
        .get('/logout', function(req, res) {
            res.redirect('/#/logout');
        })
        // API
        .get('/api/', function(req, res) {
            res.redirect('/');
        })
        .get('/api/user', function(req, res) {
            res.format({
                json: function() {
                    res.json({ page: 'user page' });
                }
            });
        })
        .get('/api/login', function(req, res) {
            res.redirect('/#/login');
        })
        .post('/api/login', function(req, res) {
            var username = req.body.username;
            var password = req.body.password;
            var method = (Validator.isEmail(username)) ? 'byEmail' : 'byUsername';
            res.format({
                json: function() {
                    res.json({
                        username: username,
                        password: password,
                        method: method
                    });
                }
            });
        });

    app.use('/', router);

    //===============PORT=================
    var port = process.env.PORT || 10000; //select your port or let it pull from your .env file
    app.listen(port);
    console.log('Application %d running on : http://localhost:%d/', cluster.worker.id, port);
}

// Listen for dying workers
cluster.on('exit', function(worker) {
    console.log('Worker %d died :( it will restart', worker.id);
    cluster.fork();
});